BEFORE USING EASY_ANALYSER

1)  You should  modify the follwoing line in easy_analyser.py 

               os.system('/Users/puma/QE_CHECK/develop/bin/gww_fit.x < fit.in > fit.out')

    inserting the full path of the gww_fit.x program which is distributed with the QE.

2)You should create a PREFIX_fit.in file with all the parameters required for the analytic continution
    where PREFIX is the same prefix name used in the pw.x and pw4gww.x calculation

   the required/used paramters are:
           ggwin%prefix='si64atoms'
                           PREFIX
           ggwin%max_i=160,
                        the same as NBND in pw.x 
           ggwin%omega_fit=10
                        the same as OMEGA_GAUSS in pw4gww.x 
           ggwin%n_grid_fit=199
                        the same as N_GAUSS in pw4gww.x MINUS ONE
           ggwin%n_fit=100,
                         in the range 1,ggwin%n_grid_fit,  number of points used for the fit 
           ggwin%n_multipoles=2
                         number of multipoles used for analytic continuation
	   ggwin%offset_fit=0
                         number of points to be discarded starting from the origin
        
/

WHEN USING EASY_ANALYSER

The program requires all the directories PREFIX-gwl_orbital_* created by pw4gww.x
and requires the output file. If the calculation has been splitted in subgroups
a file containg the concatenation of all the output files is required
 
The program asks:
      1)Name of output easy file:
          Put the name of the output file(s) of pw4gww.x
      2)Prefix
           The same prefix of pw.x and pw4gww.x
      3) KS state min
      4) KS state max
           Range of KS states for which we want to do the analysis
      5) spin min
      6) spin max
       Not spin polarised calculations: 1 1
           Spin polarised calculations: 1 2
      7)New file convention t/f:
           Put 't'
      8)Kind of Importance sampling: 0-None  1-|Psi>
           Put 1 for using the method with weighted sums
      9)Thresholds for wavefunctions
           Put here the threshold s. Note that more values are also accepted (eg. 0.  0.1 1)
     10)Distances between consecutive points 3d array
           Put here the spacing n. Note that more values are also accepted (e.g. 2 4 6)
     11)Make graphs?
           Put 'y' or 'Y' if you want on scree graphs with the calcualted self-energy 
           expectation values on imaginary frequency. Type 'n' or 'N' otherwise


The program create a table with the calculated GW energy level for every couple of chosen
threshold s and spacing n. The table gives also the number of real space grid points used.
In the directory firdir you can find the files required for the analytic continuation
relative to the last set of s and n.


  



